# 2022-01-15-SDML-mixed-precision-optimizers-relu-selu-elu-activation.md

Compare activation functions

    - ReLU
    - GELU
    - ELU
    - SELU (Swish?)

https://arxiv.org/abs/2110.02861