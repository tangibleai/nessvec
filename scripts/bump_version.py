#!/usr/bin/env python
from pathlib import Path
import re
import shutil

BASE_DIR = Path(__file__).parent.parent
PKG_DIR = BASE_DIR / 'src' / BASE_DIR.name
PYPROJECT_PATH = BASE_DIR / 'pyproject.toml'
PATTERN = re.compile(r'(version\s*=\s*)[\'"]?(\d(\.\d+)+)[\'"]?\s*')
__PATTERN__ = re.compile(r'\s*(__version__\s*=\s*)[\'"]?(\d(\.\d+)+)[\'"]?\s*.*\n')


if __name__ == '__main__':
    verline = None
    with PYPROJECT_PATH.open() as fin:
        lines = []
        for line in fin:
            lines.append(line)
            if verline:
                continue
            match = PATTERN.match(line)
            if match:
                print(f'Found match.groups(): {dict(list(enumerate(match.groups())))}')
                ver = [int(x) for x in match.groups()[1].split('.')]
                print(f'             Old ver: {ver}')
                ver[-1] += 1
                print(f'             New ver: {ver}')
                ver = '.'.join([str(x) for x in ver])
                print(f'         New ver str: {ver}')
                verline = f'version = "{ver}"\n'
                lines[-1] = verline
                print(f'        New ver line: {lines[-1]}')
    if verline:
        shutil.copy(PYPROJECT_PATH, PYPROJECT_PATH.with_suffix('.toml.bak'))
        with PYPROJECT_PATH.open('w') as fout:
            fout.writelines(lines)

    # for path in BASE_DIR.glob('**/*.py'):
    path = PKG_DIR / 'constants.py'
    if path.is_file():
        verline = None
        with path.open() as fin:
            lines = []
            verline = None
            for line in fin:
                lines.append(line)
                if verline:
                    continue
                match = __PATTERN__.match(line)
                if match:
                    print(f'version in constants: {dict(list(enumerate(match.groups())))}')
                    verstr = match.groups()[1]
                    newver = ver = [int(x) for x in verstr.split('.')]
                    print(f'             Old ver: {ver}')
                    newver[-1] += 1
                    print(f'             New ver: {newver}')
                    newver = '.'.join([str(x) for x in ver])
                    print(f'         New ver str: {newver}')
                    verline = line.replace(verstr, newver)
                    lines[-1] = verline
                    print(f'        New ver line: {lines[-1]}')
        if verline:
            shutil.copy(path, path.with_suffix('.py.bak'))
            with path.open('w') as fout:
                fout.writelines(lines)
