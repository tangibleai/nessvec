pip install virtualenv
deactivate
rm -rf .venv
python -m virtualenv .venv
source ./.venv/bin/activate
# pip install --upgrade scikit-learn
# pip install --upgrade transformers
# pip install --upgrade pandas
pip install --upgrade -e .
