## Out-of-Core processing

### Python Packages

In order of preference:

#### Dask
out-of-core dataframe, async .compute()

```bash
# install pyarror or fastparquet for parquet file format support
conda install -c conda-forge dask pyarrow
```

#### datatable
Developed by H20 for driverless.ai.
Similar to Dask but better, mem-mapped files and more R-like, Pandas-like
[Pandas operations in datatable](https://datatable.readthedocs.io/en/latest/manual/comparison_with_pandas.html)

```bash
conda install -c conda-forge datatable
```

#### pyarrow
Parquet file format

#### fastparquet
pure python parquet file format

#### pandas
chunksize


#### Datasets
simple django-like orm for postgres or sqlite

#### Vaex
postgres backend supported

#### H5py
complicated matlab like file format, heterogenous data

#### Blaze
async compute

#### pyspark
async compute

#### pymongo
mongo nosql db

### File format

- parquet (directory with partitioned data or single table file)
- dask
- hdf5 (h5py)
- protobuf

### Indexers

Conventional [KD-Tree and Ball-Tree indexes](http://jakevdp.github.io/blog/2013/04/29/benchmarking-nearest-neighbor-searches-in-python/) within databases don't even consider vectors with more than 100 dimensions. Even the Cube plugin for PostgeSQL is limited to 100 dimensions and only really works well below 50 D.

- pynndescent - fast and simple to install
- scann - [fastest](http://ann-benchmarks.com/glove-100-angular_10_angular.html) but not easy to install 
- annoy - difficult to install
- postgresql Cube (datasets uses it with ANN?)
- pgann - >100-D vectors in postgresql Cube

### Training and fine tuning

- umap-learn - scikit-learn compatible nonlinear Transformer

### pretrained word vectors 

- [`pip install zeugma`](https://pypi.org/project/zeugma/)
- [awesome-embedding-models](https://github.com/Hironsan/awesome-embedding-models)
- [Word2vec (English)](https://code.google.com/archive/p/word2vec/)
- [Word2vec (30+ languages)](https://github.com/Kyubyong/wordvectors)
- [FastText (157 languages)](https://github.com/facebookresearch/fastText/blob/master/docs/crawl-vectors.md)
- [FastText (Japanese with NEologd)](https://drive.google.com/open?id=0ByFQ96A4DgSPUm9wVWRLdm5qbmc)
- [GloVe](http://nlp.stanford.edu/projects/glove/)
- [Dependency-Based Word Embeddings](https://levyomer.wordpress.com/2014/04/25/dependency-based-word-embeddings/)
- [Meta-Embeddings](http://cistern.cis.lmu.de/meta-emb/)
- [Lex-Vec](https://github.com/alexandres/lexvec)
- [Huang et al. (2012)'s embeddings (HSMN+csmRNN)](http://stanford.edu/~lmthang/morphoNLM/)
- [Collobert et al. (2011)'s embeddings (CW+csmRNN)](http://stanford.edu/~lmthang/morphoNLM/)
- [BPEmb: subword embeddings (275 languages)](https://github.com/bheinzerling/bpemb)
- [Wikipedia2Vec entity embeddings (12 languages)](https://wikipedia2vec.github.io/wikipedia2vec/pretrained/)
- [word2vec-slim](https://github.com/eyaler/word2vec-slim)
- [BioWordVec: fastText for biomedical text](https://github.com/ncbi-nlp/BioSentVec)

### References

* Nice comparison of Vaex, PySpark, datatable, Dask, ...: https://towardsdatascience.com/beyond-pandas-spark-dask-vaex-and-other-big-data-technologies-battling-head-to-head-a453a1f8cc13
* Parquet file format for Dask: https://coiled.io/blog/parquet-file-column-pruning-predicate-pushdown/
* Dask tutorial: https://pythondata.com/dask-large-csv-python/
* Loaders and academic papers for embeddings: https://github.com/Hironsan/awesome-embedding-models
* Stanford GloVe vectors: https://nlp.stanford.edu/projects/glove/
* 400k 300d GloVe vectors: https://nlp.stanford.edu/data/glove.6B.zip
* FastText 1M 300d vectors loaded here: https://fasttext.cc/docs/en/english-vectors.html
* dataset package simple ORM sql: https://dataset.readthedocs.io/en/latest/
* word2vec c code: https://github.com/dav/word2vec/tree/master/data
* SOTA USE (universal sentence encoding) in 2018 was Elmo: https://medium.com/huggingface/universal-word-sentence-embeddings-ce48ddc8fc3a

